var tableCols = [
    {
        name: 'company',
        title: 'Quest Рум',
        type: 'input-text',
        validation: /^[a-zа-я0-9 ]{3,}/i,  //min length 3 characters
        sort: 'text'
    },
    {
        name: 'name',
        title: 'Игра',
        type: 'input-text',
        validation: /^[a-zа-я0-9 ]{3,}/i,  //min length 3 characters
        sort: 'text'
    },
    {
        name: 'date',
        title: 'Дата',
        type: 'input-date',
        sort: 'date'
    },
    {
        name: 'order',
        title: 'Порядок',
        type: 'input-number',
        validation: /[0-9]/i,  //min length 3 characters,
        sort: 'number'
    },
    {
        name: 'timeSpent',
        title: ' Время',
        type: 'input-text',
        validation: /^[0-5]?[0-9]$/,
        sort: 'number'
    },
    {
        name: 'users',
        title: 'Квестеры',
        type: 'input-text',
        sort: 'number'
    },
    {
        name: 'status',
        title: 'Статус',
        type: 'select',
        sort: 'number',
        list: [{
            id: 0,
            name: 'На очереди'
        },{
            id: 1,
            name: 'Пройдена'
        },
        {
            id: 2,
            name: 'Fail!'
        }]
    },
    {
        name: 'isOpen',
        title: 'Состояние комнаты',
        type: 'select',
        sort: 'number',
        list: [{
            id: 0,
            name: 'Закрыта/ В разработке'
        },{
            id: 1,
            name: 'Открыта'
        }]
    }
];

var EManager = function () {
    this.events = {};

    this.emit = function (event, params) {
        if (!this.events[event]) {
            return;
        }

        this.events[event].forEach(function (listener) {
            listener(params);
        });
    };

    this.on = function (event, listener) {
        if (!this.events.hasOwnProperty(event)) {
            this.events[event] = [];
        }

        this.events[event].push(listener);
    };
};

var EventManager = new EManager();

class Xhr {
    constructor() {
        this._xhr = new XMLHttpRequest();
        this._xhr.onreadystatechange = this.stateChange.bind(this);
        this._listeners = {
            success: [],
            failure: []
        };
    }
    stateChange() {
        if (this._xhr.readyState != 4) {
            return;
        }

        if (this._xhr.status == 200) {
            this._fire('success', this._xhr.responseText);
        } else {
            this._fire('failure', this._xhr.status.toString());
        }
    }

    on(event, listener) {
        if (!this._listeners[event]) {
            return;
        }

        this._listeners[event].push(listener);
    }

    _fire(event, data) {
        this._listeners[event].forEach((listener) => listener(data));
    }

}

class QuestLoader extends Xhr {
    load() {
        this._xhr.open('GET', 'http://qom.com.ua/api/quests', true);
        this._xhr.send(null);
    }
}

class QuestSaver extends Xhr {
    save(data) {
        this._xhr.open('POST', 'http://qom.com.ua/api/quest', true);
        this._xhr.send(JSON.stringify(data));
    }
}

class QuestRemover extends Xhr {
    remove(id) {
        this._xhr.open('DELETE', 'http://qom.com.ua/api/quest/' + id, true);
        this._xhr.send(null);
    }
}
var FManager = function () {
    this.counterForNewTempRow = 0;

    this.questLoader = new QuestLoader();
    this.questLoader.load();
    this.questLoader.on('success', function (quests) {
        this.data = JSON.parse(quests);
        EventManager.emit('updateQuestData', {
            data: this.data,
            rawdata: [].concat(this.data)
        });
    }.bind(this));


    this.filters = {
        text: '',
        startDate: null,
        endDate: null,
        isOpen: '',
        status: ''
    };

    this.fireFilters = function () {
        var data = [].concat(this.data);
        Object.keys(this.filters).map(function (filter) {

            var filters = this.filters;

            if (!filters[filter]) {
                return;
            }

            switch (filter) {
                case 'text':
                    data = data.filter(function(record) {
                        var regex = new RegExp(filters[filter], 'gi');
                        if (regex.test(record.company) || regex.test(record.name)) {
                            return record;
                        }
                    });
                break;

                case 'startDate':
                case 'endDate':
                    data = data.filter(function(record) {
                        var date = new Date(moment(record.date,"DD/MM/YYYY").format());
                        if (filters.startDate == null || filters.endDate == null) {
                            return record;
                        }
                        if (filters.startDate != null && filters.endDate != null) {
                            if (date >= new Date(filters.startDate.format()) && date <= new Date(filters.endDate.format())) {
                                return record;
                            }
                        }
                    });
                break;

                case 'isOpen':
                case 'status':
                    data = data.filter(function(record) {
                        var regex = new RegExp(filters[filter], 'gi');
                        if (regex.test(record[filter])) {
                            return record;
                        }
                    });
                break;
            }

        }.bind(this));
        EventManager.emit('updateQuestData', {
            data: data
        });
    };

    EventManager.on('questSearch', (searchValue) => {
        this.filters.text = searchValue;
        this.fireFilters();
    });

    EventManager.on('questDateSearch', (date) => {
        this.filters.startDate = date[0];
        this.filters.endDate = date[1];
        this.fireFilters();
    });

    EventManager.on('questSelectSearch', (searchBySelect) => {
        if (searchBySelect[0] == 'all') {
            searchBySelect[0] = ''
        }
        this.filters[searchBySelect[1]] = searchBySelect[0];
        this.fireFilters();
    });

    EventManager.on('questAdd', (arg) => {
        var data = this.data;

        this.counterForNewTempRow++;
        arg.id = 'temp' + this.counterForNewTempRow;
        data.unshift(arg);

        var copyStateValidation = {};
        copyStateValidation['company'] = true;
        copyStateValidation['name'] = true;

        EventManager.emit('updateQuestData', {
            data: data,
            rawdata: [].concat(data),
            validation: copyStateValidation
        });
    });

    EventManager.on('questDelete', (arg) => {
        var data = this.data;
        var recordToDelete;

        Object.keys(data).map(function (questId, index) {
            if (data[questId].id == arg) {
                recordToDelete = index;
            }
        });

        data.splice(recordToDelete, 1);


        EventManager.emit('updateQuestData', {
            data: data,
            rawdata: [].concat(data)
        });
    });

    EventManager.on('updateQuestItemData', (rowdata) => {
        var data = this.data;

        Object.keys(data).map(function (item) {
            if (data[item].id == rowdata.id) {
                data[item] = rowdata;
            }
        }.bind(this));

        EventManager.emit('updateQuestData', {
            data: data,
            rawdata: [].concat(data)
        });
    });

    EventManager.on('resetAllFilters', () => {

        EventManager.emit('resetDateRangePicker');
        EventManager.emit('resetTextSearch');
        EventManager.emit('resetSelectFilters');
        EventManager.emit('updateQuestData', {
            data: this.data
        });
    });
};

var FilterManager = new FManager();

var Filters = React.createClass({
    render: function () {
        return (
            <div className="filters">
                <div className="filters-top">
                    <TableSelectors />
                    <ResetAllFiltersButton />
                </div>
                <div className="filters-bottom">
                    <SearchContainer />
                    <AddNewRowButton />
                </div>
            </div>
        );
    }
});

var SearchContainer = React.createClass({

    getInitialState: function () {
        return {
            text: ''
        }
    },

    componentDidMount: function () {
        EventManager.on('resetTextSearch', (textfilter) => {
            this.setState({
                text: ''
            });
        });
    },

    onSearchChange: function (event) {
        var value = event.target.value;
        this.setState({
            text: value
        });
        EventManager.emit('questSearch', value);
    },

    render: function () {
        return (
            <div className="search-container">
                <i className="icon-search"></i>
                <input type="text" placeholder="Найти рум или игру" value={this.state.text} onChange={this.onSearchChange}/>
            </div>
        );
    }
});

var TableSelectors = React.createClass({

    getInitialState: function () {
        return {
            isOpen: 'all',
            status: 'all'
        }
    },

    componentDidMount: function () {
        EventManager.on('resetSelectFilters', () => {
            this.setState({
                isOpen: 'all',
                status: 'all'
            });
        });
    },

    onSelectChange: function (event) {
        var value = event.target.value;
        var select = event.target.getAttribute('data-filter');

        this.setState({
            [select]: value
        });
        EventManager.emit('questSelectSearch', [value, select]);
    },

    render: function () {
        return (
            <div className="selectors-container">
                <span className="filter-label">Фильтры:</span>
                <div className="selectors-block">
                    <select data-filter="isOpen" value={this.state.isOpen} onChange={this.onSelectChange}>
                        <option value="all"> Все Quest Румы</option>
                        <option value="0"> Закрыта / В разработке</option>
                        <option value="1"> Открыта</option>
                    </select>
                </div>
                <div className="selectors-block">
                    <select data-filter="status" value={this.state.status} onChange={this.onSelectChange}>
                        <option value="all"> Все Игры</option>
                        <option value="0"> На очереди </option>
                        <option value="1"> Пройдена</option>
                        <option value="2"> Fail!</option>
                    </select>
                </div>
                <DateRangePicker />
            </div>
        );
    }
});

var DateRangePicker = React.createClass({

    getInitialState: function () {
        return {
            startDate: null,
            endDate: null
        }
    },

    componentDidMount: function () {
        EventManager.on('resetDateRangePicker', () => {
            this.setState({
                startDate: null,
                endDate: null
            });
        });
    },

    handleChange: function ({ startDate, endDate }) {
        startDate = startDate || this.state.startDate;
        endDate = endDate || this.state.endDate;

        if (startDate.isAfter(endDate)) {
            var temp = startDate;
            startDate = endDate;
            endDate = temp
        }

        this.setState({ startDate, endDate });

        EventManager.emit('questDateSearch', [startDate, endDate] );

    },

    handleChangeStart: function (startDate) {
        this.handleChange({ startDate })
    },

    handleChangeEnd: function (endDate) {
        this.handleChange({ endDate })
    },

    onDateRangeReset: function () {
        this.setState({
            startDate: null,
            endDate: null
        });
        EventManager.emit('questDateSearch', [null, null] );
    },

    render: function () {
        return (
            <div className="search-wrapper">
                <span className="date-range-label">С</span>
                <DatePicker
                    selected={this.state.startDate}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    onChange={this.handleChangeStart} />
                <span className="date-range-label">По</span>
                <DatePicker
                    selected={this.state.endDate}
                    startDate={this.state.startDate}
                    endDate={this.state.endDate}
                    onChange={this.handleChangeEnd} />
                <i onClick={this.onDateRangeReset} className="icon-event_busy"></i>
            </div>
        );
    }
});

var ResetAllFiltersButton = React.createClass({
    onResetButtonClick: function () {
        EventManager.emit('resetAllFilters');
    },

    render: function () {
        return (
            <button type="button" className="action-button icon-cancel" onClick={this.onResetButtonClick}></button>
        );
    }
});

var AddNewRowButton = React.createClass({
    onAddButtonClick: function () {
        var newData = {
            id: '',
            company: '',
            name: '',
            date: moment().format('DD.MM.YYYY'),
            order: '',
            timeSpent: 0,
            users: '',
            status: 0,
            isOpen: 0
        };

        EventManager.emit('questAdd', newData);
    },

    render: function () {
        return (
            <button type="button" className="add-button icon-control_point" onClick={this.onAddButtonClick}> </button>
        );
    }
});

var Table = React.createClass({
    //getInitialState: function () {
    //
    //    return {
    //        tableHeight: 0
    //    }
    //},
    //componentDidMount: function () {
    //    window.addEventListener('resize', this.handleResize);
    //},
    //componentWillUnMount: function () {
    //    window.removeEventListener('resize', this.handleResize);
    //},
    //
    //componentDidUpdate: function () {
    //    var scrollableTable = document.getElementById('scrollableTable');
    //    console.log(scrollableTable)
    //    debugger;
    //},

    handleResize: function(e) {
        //
    },
    render: function () {

        var header = <Header staticCells={this.props.staticCells}/>;
        var tbody = <TBody staticCells={this.props.staticCells}/>;

        return (
            <div id="flip-scroll">
                <table>
                    <thead>{header}</thead>
                </table>
                <div id="scrollableTable" className="scrollable-table">
                    <table>
                        {tbody}
                    </table>
                </div>
                <div className="fixed-table-container">
                    <TotalRow />
                </div>
            </div>
        );
    }
});

var Header = React.createClass({

    sortColumn: function (event) {
        var sortProperty =  event.target.getAttribute('data-sort');
        var sortType = event.target.getAttribute('data-sort-type');
        if (sortProperty == null || sortType == null) {
            sortProperty = event.target.parentElement.parentElement.getAttribute('data-sort');
            sortType = event.target.parentElement.parentElement.getAttribute('data-sort-type');
        }
        EventManager.emit('sortColumn', [sortProperty, sortType]);
    },

    render: function () {

        var headerCells =[];

        {Object.keys(tableCols).map(function (cell, index) {
            headerCells.push(<th key={index}><a data-sort-type={tableCols[cell]['sort']} data-sort={tableCols[cell]['name']} onClick={this.sortColumn} >{tableCols[cell]['title']}
                <span><i className="icon-arrow_drop_up"></i><i className="icon-arrow_drop_down"></i></span></a></th>);
        }.bind(this))}
        if (this.props.staticCells) {
            headerCells.push(<th key={headerCells.length + 1}>{this.props.staticCells}</th>)
        }

        return (
            <tr id="questHeaderCells">
                {headerCells}
            </tr>
        );
    }
});

var TBody = React.createClass({
    getInitialState: function () {

        this.counterForNewTempRow = 0;

        return {
            data: {},
            rawdata: {},
            sort: null
        }
    },

    componentDidMount: function() {
        window.addEventListener('resize', this.handleResize);

        EventManager.on('sortColumn', (sort) => {

            var data = Object.assign(this.state.data);

            function sortBy (sort) {
                var property = sort[0];
                var type = sort[1];
                switch (type){
                    case 'text':
                        return function (a,b) {
                            return (a[property].toUpperCase() < b[property].toUpperCase()) ? -1 : (a[property].toUpperCase() > b[property].toUpperCase()) ? 1 : 0;
                        };
                    break;
                    case 'number':
                        return function (a,b) {
                            return (parseFloat(a[property]) < parseFloat(b[property])) ? -1 : (parseFloat(a[property]) > parseFloat(b[property])) ? 1 : 0;
                        };
                    break;
                    case 'date':
                        return function (a,b) {
                            return (new Date(moment(a[property],"DD/MM/YY").format()) < new Date(moment(b[property],"DD/MM/YY").format())) ? -1 : (new Date(moment(a[property],"DD/MM/YY").format()) > new Date(moment(b[property],"DD/MM/YY").format())) ? 1 : 0;
                        };
                    break;
                    default:
                        return function (a,b) {
                            return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                        };
                    break;
                }

            }

            if (this.state.sort == 'ASC') {
                data.sort(sortBy(sort));
                this.setState({
                    data: data,
                    sort: 'DESC'
                })
            } else {
                data.sort(sortBy(sort)).reverse();
                this.setState({
                    data: data,
                    sort: 'ASC'
                })
            }
        });

        EventManager.on('updateQuestData', (data) => {
            this.setState(data);

            this.countTotalFilteredTime(data);

            EventManager.emit('setTotalRowValues', {
                rowsCount: data.data.length,
                totalTimeSpent: this.countTotalFilteredTime(data)
            });

            if (data.rawdata) {
                EventManager.emit('setQuestGamesQty', {
                    pastGames: data.rawdata.length - this.calculateFutureQuestGamesQty(data.rawdata),
                    futureGames: this.calculateFutureQuestGamesQty(data.rawdata)
                });
            }
        });

    },

    componentWillUnMount: function () {
        window.removeEventListener('resize', this.handleResize);
    },

    handleResize: function(e) {
        this.setCellsWidth();
        this.setTableHeight();
    },

    componentDidUpdate: function () {
        this.setCellsWidth();
        this.setTableHeight();
    },

    setTableHeight: function () {
        var scrollableTable = document.getElementById('scrollableTable');
        var totalRow = document.getElementsByClassName('fixed-table-container')[0];
        var height = window.innerHeight - scrollableTable.getBoundingClientRect().top - totalRow.clientHeight;
        scrollableTable.style.height = height + 'px';
    },

    setCellsWidth: function () {
        var firstRow = document.getElementById('questFirstRow');
        var headerCells = document.getElementById('questHeaderCells');
        if (firstRow) {
            for (var i = 0; i < firstRow.children.length; i++) {
                headerCells.children[i].style.width = firstRow.children[i].clientWidth + 'px';
            }
        }
    },

    calculateFutureQuestGamesQty: function (data) {
        var count = 0;
        var data = [].concat(data);
        Object.keys(data).map(function (record) {
            if (data[record].status == 0) {
                count += 1;
            }
        }.bind(this));
        return count;
    },

    countTotalFilteredTime: function (data) {

        var copydata = [].concat(data.data);
        var timeCounter = 0;

        copydata = copydata.filter(function(record) {
            timeCounter += parseFloat(record.timeSpent);
        });

        if (timeCounter > 59) {
            return  Math.floor(timeCounter / 60) + 'ч ' + (timeCounter % 60) + 'мин';
        } else {
            return timeCounter + 'мин';
        }
    },

    render: function () {
        return (
            <tbody style={this.state.tableStyles}>
                {Object.keys(this.state.data).map(function (questId, index) {
                    return <TableRow isFirstRow={index == 0} defaultValidation={this.state.validation}  key={this.state.data[questId].id} dataid={this.state.data[questId].id} datarow={this.state.data[questId]} staticCells={this.props.staticCells}/>;
                }.bind(this))}
            </tbody>
        );
    }
});

var TableRow = React.createClass({
        getInitialState: function() {
            this.questSaver = new QuestSaver();
            this.questRemover = new QuestRemover();
            this.changedRowData = this.props.datarow;

            this.questSaver.on('success', function (questId) {
                if (typeof +questId == 'number' && +questId != 0) {
                    this.changedRowData.id = +questId;
                }

                this.setState({
                    editMode: false,
                    datarow: this.changedRowData
                });

                EventManager.emit('updateQuestItemData', this.changedRowData);

            }.bind(this));

            this.questRemover.on('success', function () {
                EventManager.emit('questDelete', [this.changedRowData.id]);
            }.bind(this));


            return {
                editMode: false,
                datarow: Object.assign({}, this.props.datarow),
                validation: this.props.defaultValidation || {},
                startDate: moment(this.props.datarow.date, "DD.MM.YYYY")
            };
        },

        onKeyPress: function (target) {
            if (target.charCode==13) {
                this.onAcceptRowValues();
            }
        },

        onAcceptRowValues: function () {

            this.changedRowData['date'] = moment(this.state.startDate).format("DD.MM.YYYY");

            if (Object.keys(this.state.validation).length > 0) {
                return;
            } else {
                this.setState({
                    editMode: false
                });
                this.questSaver.save(this.changedRowData);
            }
        },

        onInputChange: function (e) {
            var fieldName = e.target.getAttribute('data-field');
            var regex;
            var copyStateValidation = Object.assign(this.state.validation);

            tableCols.map(function (column, index) {
                if(column['name'] == fieldName) {
                    regex = column['validation'];

                }
            }.bind(this));
            if(regex) {
                if (regex.test(e.target.value)) {
                    this.changedRowData[fieldName] = e.target.value;
                    delete copyStateValidation[fieldName]
                } else {
                    copyStateValidation[fieldName] = true;
                }
            } else {
                this.changedRowData[fieldName] = e.target.value;
            }

            this.setState({
                validation: copyStateValidation
            });
        },

        onDateChange: function(date) {
            this.setState({
                startDate: date
            });

        },

        onDeleteRow: function () {
            if(/temp/.test(this.state.datarow.id)) {
                EventManager.emit('questDelete', [this.state.datarow.id]);
            } else {
                this.questRemover.remove(this.state.datarow.id);
            }
        },

        render: function () {
            var cells = [];

            Object.keys(tableCols).map(function (column, index) {
                Object.keys(this.state.datarow).map(function (cell, index) {

                    var indexKey = index;

                    if (tableCols[column]['name'] == cell) {
                        if (/temp/.test(this.state.datarow.id) || this.state.editMode) {
                            if (tableCols[column]['type'] == 'select') {
                                cells.push(<td key={index} data-title={tableCols[column]['title']}>
                                    <div className="select-box">
                                        <select id={'select' + index} data-field={cell} defaultValue={this.state.datarow[cell]} onChange={this.onInputChange}>
                                            {Object.keys(tableCols[column]['list']).map(function (option, index) {
                                                    return <option key={index} value={option}> {tableCols[column]['list'][option]['name']}</option>

                                            }.bind(this))}
                                        </select>
                                    </div>
                                    </td>)
                            } else if (tableCols[column]['type'] == 'input-date') {
                                cells.push(<td data-title={tableCols[column]['title']} key={indexKey} className="quest-datepicker">
                                    <DatePicker
                                        data-field={cell}
                                        popoverTargetAttachment='top center'
                                        popoverAttachment='top center'
                                        popoverTargetOffset='0px 0px'
                                        dateFormat="DD.MM.YYYY"
                                        selected={this.state.startDate}
                                        onChange={this.onDateChange} />
                                </td>)
                            } else {
                                cells.push(<td key={indexKey} data-title={tableCols[column]['title']}><input onKeyPress={this.onKeyPress} data-error={(this.state.validation[cell] ? true : false)} data-field={cell} defaultValue={this.state.datarow[cell]} onChange={this.onInputChange} type="text"/></td>)
                            }
                        } else {
                            if (tableCols[column]['type'] == 'select') {
                                {Object.keys(tableCols[column]['list']).map(function (value, index) {
                                    if (this.state.datarow[cell] == value) {
                                        cells.push(<td key={indexKey} data-title={tableCols[column]['title']}> {tableCols[column]['list'][value]['name']}</td>);
                                    }
                                }.bind(this))}
                            } else {
                                cells.push(<td key={indexKey} data-title={tableCols[column]['title']}>{this.state.datarow[cell]}</td>)
                            }
                        }
                    }
                }.bind(this))
            }.bind(this));


            if (this.props.staticCells) {
                if (/temp/.test(this.state.datarow.id) || this.state.editMode) {
                    if(/temp/.test(this.state.datarow.id)) {
                        cells.push(<td key={cells.length + 1} data-title={this.props.staticCells}>
                            <button type="button" onClick={this.onAcceptRowValues} className="iconic-button icon-done confirm-button"> </button>
                            <button type="button" className="iconic-button icon-delete" onClick={this.onDeleteRow}> </button>
                        </td>)
                    } else {
                        cells.push(<td key={cells.length + 1} data-title={this.props.staticCells}>
                            <button type="button" onClick={this.onAcceptRowValues} className="iconic-button icon-done confirm-button"> </button>
                            <button type="button" onClick={() => {
                            this.setState({
                                editMode: false
                            })
                        }} className="iconic-button cancel-button icon-close"> </button>
                        </td>)
                    }
                } else {
                    cells.push(<td key={cells.length + 1} data-title={this.props.staticCells}>
                        <button type="button" onClick={() => {
                            this.changedRowData = Object.assign({}, this.state.datarow);
                            this.setState({
                                editMode: true
                            })
                        }} className="iconic-button icon-mode_edit"> </button>
                        <button type="button" className="iconic-button icon-delete" onClick={this.onDeleteRow}> </button>
                    </td>)
                }
            }

            return (
                <tr id={this.props.isFirstRow ? 'questFirstRow' : ''}>
                    {cells}
                </tr>
            );
        }
    });

var TotalRow =React.createClass({
    getInitialState: function() {
        return {
            rowsCount: 0,
            totalTimeSpent: 0
        };
    },

    componentDidMount: function () {
        EventManager.on('setTotalRowValues', (questCount) => {
            this.setState(questCount);
        });
    },

    render: function () {

        return (
            <table>
                <tbody>
                <tr>
                    <td data-title="Quest Рум"> </td>
                    <td data-title="Комната">{this.state.rowsCount}</td>
                    <td data-title="Дата"> </td>
                    <td data-title="Порядок"> </td>
                    <td data-title="Время">{this.state.totalTimeSpent}</td>
                    <td data-title="Квестера"> </td>
                    <td data-title="Статус">{this.state.rowsCount}</td>
                    <td data-title="Состояние комнаты">{this.state.rowsCount}</td>
                    <td data-title="Действия"> </td>
                </tr>
                </tbody>
            </table>
        )
    }
});

var PageHeader = React.createClass({

    getInitialState: function () {
        return {
            pastGames: 0,
            futureGames: 0
        }
    },

    componentDidMount: function () {
        EventManager.on('setQuestGamesQty', (data) => {
            this.setState(data);
        });
    },

    render: function () {

        return (
            <header>
                <div className="quest-info">
                    <div className="quest-info__block">
                        <i className="icon-lock_open"> </i>
                        <span>{this.state.pastGames}</span>
                    </div>
                    <div className="quest-info__block">
                        <i className="icon-lock_outline"> </i>
                        <span>{this.state.futureGames}</span>
                    </div>
                </div>
                <div className="user-profile">
                    <span className="user-profile__username">User Name</span>
                    <span className="user-profile__useravatar">
                        <img src="http://lorempixel.com/100/100/people/1/" alt=""/>
                    </span>
                </div>
            </header>
        )
    }

});

var Layout = React.createClass({
    render: function () {
        var filters = <Filters />;
        var table = <Table staticCells="Действия" />;
        var pageHeader = <PageHeader />;
        return (
            <div>
                {pageHeader}
                <div className="content">
                    {filters}
                    {table}
                </div>
            </div>
        );
    }
});

ReactDOM.render(
    <Layout/>,
    document.getElementById('wrapper')
);